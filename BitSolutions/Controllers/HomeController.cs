﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Specialized;

namespace BitSolutions.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PagamentoUmMes()
        {
            return View();
        }

        public ActionResult Pagamento(Int32 meses)
        {
            String result = String.Empty;

            using (WebClient client = new WebClient())
            {
                byte[] response =
                client.UploadValues("https://ws.pagseguro.uol.com.br/v2/sessions", new NameValueCollection()
                {
                   { "email", "marcelogando@hotmail.com" },
                   { "token", "B69668C2B036443882408B6540027842" }
                });

                result = System.Text.Encoding.UTF8.GetString(response);
            }
            
            //using (WebClient client = new WebClient())
            //{
            //    byte[] response =
            //    client.UploadValues("https://ws.pagseguro.uol.com.br/v2/sessions", new NameValueCollection()
            //    {
            //       { "email", "suporte@lojamodelo.com.br" },
            //       { "token", "57BE455F4EC148E5A54D9BB91C5AC12C" }
            //    });

            //    result = System.Text.Encoding.UTF8.GetString(response);
            //}

            //String url = "https://ws.pagseguro.uol.com.br/v2/sessions?email=marcelogando@hotmail.com&token=B69668C2B036443882408B6540027842";
            //String url = "https://ws.sandbox.pagseguro.uol.com.br/v2/sessions";
            //String dataToPost = "email=marcelogando@hotmail.com\\&token=B69668C2B036443882408B6540027842";            //var data = Encoding.ASCII.GetBytes(dataToPost);

            //var request = (HttpWebRequest)WebRequest.Create(url);
            //request.Method = "POST";
            //request.ContentType = "application/xml";
            //request.ContentLength = data.Length;

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(data, 0, data.Length);
            //}

            //var response = (HttpWebResponse)request.GetResponse();
            string pattern = "<id>(.*?)<\\/id>";

            //String texto = new StreamReader(response.GetResponseStream(), System.Text.Encoding.GetEncoding("Windows-1252")).ReadToEnd();

            Match match = Regex.Match(result, pattern);
            String SessionId = match.Groups[1].ToString();

            ViewBag.SessionId = SessionId;

            switch (meses)
            {
                case 1:
                    ViewBag.Valor = 129.00;
                    break;
                case 3:
                    ViewBag.Valor = 159.00;
                    break;
                case 6:
                    ViewBag.Valor = 199.00;
                    break;
            }

            return View();
        }

        public ActionResult Pagar(String metodo)
        {
            String result = String.Empty;

            using (WebClient client = new WebClient())
            {
                byte[] response =
                client.UploadValues("https://ws.pagseguro.uol.com.br/v2/transactions", new NameValueCollection()
                {
                   { "email", "marcelogando@hotmail.com" },
                   { "token", "B69668C2B036443882408B6540027842" },
                    {"paymentMode","default" },
                    {"paymentMethod","boleto" },
                    {"receiverEmail","suporte@lojamodelo.com.br" },
                    {"currency","BRL" },
                    {"extraAmount","1.00" },
                    {"itemId1","0001" },
                    {"itemDescription1","Produto PagSeguroI" },
                    {"itemAmount1","99999.99" },
                    {"itemQuantity1","1" },
                    {"notificationURL","https://sualoja.com.br/notifica.html" },
                    {"reference","REF1234" },
                    {"senderName","Jose Comprador" },
                    {"senderCPF","11475714734" },
                    {"senderAreaCode","99" },
                    {"senderPhone","99999999" },
                    {"senderEmail","comprador@uol.com.br" },
                    {"shippingAddressStreet","Av.PagSeguro" },
                    {"shippingAddressNumber","9999" },
                    {"shippingAddressComplement","99o andar" },
                    {"shippingAddressDistrict","Jardim Internet" },
                    {"shippingAddressPostalCode","99999999" },
                    {"shippingAddressState","SP" },
                    {"shippingAddressCountry","ATA" },
                    {"shippingType","1" },
                    {"shippingCost","1.00" }
                });

                result = System.Text.Encoding.UTF8.GetString(response);
            }

            return RedirectToAction("Index");
        }
    }
}