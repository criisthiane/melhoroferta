﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BitSolutions.Models;
using Digital_Fortress;
using System.Configuration;

namespace BitSolutions.Controllers
{
    public class LoginController : Controller
    {
        UsuarioContext ctxUsuario = new UsuarioContext();
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult EfetuaLogin()
        {
            UsuarioETT usuarioTela = new UsuarioETT();
            usuarioTela.Usuario = Request["txtUsuario"];
            usuarioTela.Senha = Request["txtSenha"];

            UsuarioETT usuarioLogin = ctxUsuario.LoginUsuario(usuarioTela.Usuario);

            if (usuarioTela.Senha.Equals(Encrypt_Decrypt.Decrypt(usuarioLogin.Senha, ConfigurationManager.AppSettings["Passphrase"])))
            {
                return RedirectToAction("Index", "Dashboard", new { Id_Usuario = usuarioLogin.Id_Usuario, Id_Site = usuarioLogin.Id_Site });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
