﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BitSolutions.Models;
using Digital_Fortress;
using System.Configuration;

namespace BitSolutions.Controllers
{
    public class CriaUsuarioController : Controller
    {
        UsuarioContext ctxUsuario = new UsuarioContext();

        public ActionResult Index(String token)
        {
            token = Encrypt_Decrypt.Encrypt("1", ConfigurationManager.AppSettings["Passphrase"]);

            Int32 Id_Usuario;
            if (Int32.TryParse(Encrypt_Decrypt.Decrypt(token, ConfigurationManager.AppSettings["Passphrase"]), out Id_Usuario))
            {
                return View(Id_Usuario);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Criar()
        {
            UsuarioETT usuario = new UsuarioETT();
            usuario.Id_Usuario = Convert.ToInt32(Request["hfId_Usuario"]);
            usuario.Nome = Request["txtNome"];
            usuario.Email = Request["txtEmail"];
            usuario.Usuario = Request["txtUsuario"];
            usuario.Senha = Request["txtSenha"];
            usuario.Senha = Encrypt_Decrypt.Encrypt(usuario.Senha, ConfigurationManager.AppSettings["Passphrase"]);
            
            ctxUsuario.AtualizaUsuario(usuario);

            return RedirectToAction("Index", "Login");
        }
    }
}
