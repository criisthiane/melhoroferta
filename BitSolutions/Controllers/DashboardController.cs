﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BitSolutions.Models;

namespace BitSolutions.Controllers
{
    public class DashboardController : Controller
    {
        DashboardContext ctxDash = new DashboardContext();
                
        public ActionResult Index(Int32 Id_Usuario, Int32? Id_Site)
        {
            ctxDash = ctxDash.Retorna_Dashboard(Id_Usuario, Id_Site);

            return View(ctxDash);
        }

        [HttpPost]
        public ActionResult AdicionaSite()
        {
            SiteETT site = new SiteETT();
            site.Id_Usuario = Convert.ToInt32(Request["hfId_Usuario"]);
            site.Site = Request["txtNovoSite"];

            site.Id_Site = ctxDash.AdicionaSite(site);

            return RedirectToAction("Index", new { Id_Usuario = site.Id_Usuario, Id_Site = site.Id_Site });
        }

        [HttpPost]
        public ActionResult AdicionaBuscaGoogle()
        {
            RankingGoogleETT ranking = new RankingGoogleETT();
            ranking.Id_Usuario = Convert.ToInt32(Request["hfId_Usuario"]);
            ranking.Id_Site = Convert.ToInt32(Request["hfId_Site"]);
            ranking.Busca = Request["txtNovoPrdGoogle"];

            ctxDash.AdicionaBuscaGoogle(ranking);

            return RedirectToAction("Index", new { Id_Usuario = ranking.Id_Usuario, Id_Site = ranking.Id_Site });
        }

        [HttpPost]
        public ActionResult AdicionaPrecoConcorrencia()
        {
            PrecoConcorrenciaETT preco = new PrecoConcorrenciaETT();
            preco.Id_Usuario = Convert.ToInt32(Request["hfId_Usuario"]);
            preco.Id_Site = Convert.ToInt32(Request["hfId_Site"]);
            preco.Produto = Request["txtNomeProduto"];
            preco.Link_Produto_Usuario = Request["txtLinkSeu"];
            preco.Link_Produto_Concorrencia = Request["txtLinkConc"];

            ctxDash.AdicionaPrecoConcorrencia(preco);

            return RedirectToAction("Index", new { Id_Usuario = preco.Id_Usuario, Id_Site = preco.Id_Site });
        }

        [HttpPost]
        public ActionResult AdicionaComparacaoImagem()
        {
            ImagemConcorrenciaETT imagem = new ImagemConcorrenciaETT();
            imagem.Id_Usuario = Convert.ToInt32(Request["hfId_Usuario"]);
            imagem.Id_Site = Convert.ToInt32(Request["hfId_Site"]);
            imagem.Produto = Request["txtNomeProdutoImagem"];
            imagem.Link_Imagem_Usuario = Request["txtLinkSuaImagem"];
            imagem.Link_Imagem_Concorrencia = Request["txtLinkImagemConc"];

            ctxDash.AdicionaComparacaoImagem(imagem);

            return RedirectToAction("Index", new { Id_Usuario = imagem.Id_Usuario, Id_Site = imagem.Id_Site });
        }
    }
}