﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitSolutions.Models
{
    public class SiteETT
    {
        private Int32 _id_Site;
        private Int32 _id_Usuario;
        private String _site;
        private DateTime _dt_Ins;
        private DateTime _dt_Upd;

        public int Id_Site
        {
            get
            {
                return _id_Site;
            }

            set
            {
                _id_Site = value;
            }
        }

        public int Id_Usuario
        {
            get
            {
                return _id_Usuario;
            }

            set
            {
                _id_Usuario = value;
            }
        }

        public string Site
        {
            get
            {
                return _site;
            }

            set
            {
                _site = value;
            }
        }

        public DateTime Dt_Ins
        {
            get
            {
                return _dt_Ins;
            }

            set
            {
                _dt_Ins = value;
            }
        }

        public DateTime Dt_Upd
        {
            get
            {
                return _dt_Upd;
            }

            set
            {
                _dt_Upd = value;
            }
        }
    }
}