﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace BitSolutions.Models
{
    public class UsuarioContext
    {
        public UsuarioETT LoginUsuario(String Usuario)
        {
            UsuarioETT usuario = new UsuarioETT();

            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["GamaExp"].ConnectionString;
                String strProcedure = "Sp_Sel_Login";

                using (MySqlConnection conn = new MySqlConnection(connString))
                {
                    conn.ConnectionString = connString;

                    conn.Open();
                    MySqlCommand comm = new MySqlCommand(strProcedure, conn);
                    comm.CommandType = CommandType.StoredProcedure;
                    
                    MySqlParameter p_Usuario = new MySqlParameter("p_Usuario", MySqlDbType.VarChar, 100);
                    p_Usuario.Value = Usuario;
                    comm.Parameters.Add(p_Usuario);
                   
                    MySqlDataReader dr = comm.ExecuteReader();

                    while (dr.Read())
                    {
                        usuario.Id_Usuario = Convert.ToInt32(dr["Id_Usuario"]);
                        usuario.Nome = dr["Nome_Completo"].ToString();
                        usuario.Email = dr["Email"].ToString();
                        usuario.Usuario = dr["Usuario"].ToString();
                        usuario.Senha = dr["Senha"].ToString();
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            usuario.Id_Site = Convert.ToInt32(dr["Id_Site"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return usuario;
        }

        public void AtualizaUsuario(UsuarioETT Usuario)
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["GamaExp"].ConnectionString;
                String strProcedure = "Sp_Upd_Usuario";

                using (MySqlConnection conn = new MySqlConnection(connString))
                {
                    conn.ConnectionString = connString;

                    conn.Open();
                    MySqlCommand comm = new MySqlCommand(strProcedure, conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    MySqlParameter p_Id_Usuario = new MySqlParameter("p_Id_Usuario", MySqlDbType.Int32);
                    p_Id_Usuario.Value = Usuario.Id_Usuario;
                    comm.Parameters.Add(p_Id_Usuario);

                    MySqlParameter p_Nome_Completo = new MySqlParameter("p_Nome_Completo", MySqlDbType.VarChar, 500);
                    p_Nome_Completo.Value = Usuario.Nome;
                    comm.Parameters.Add(p_Nome_Completo);

                    MySqlParameter p_Genero = new MySqlParameter("p_Genero", MySqlDbType.VarChar, 50);
                    p_Genero.Value = Usuario.Genero;
                    comm.Parameters.Add(p_Genero);

                    MySqlParameter p_Email = new MySqlParameter("p_Email", MySqlDbType.VarChar, 100);
                    p_Email.Value = Usuario.Email;
                    comm.Parameters.Add(p_Email);

                    MySqlParameter p_Usuario = new MySqlParameter("p_Usuario", MySqlDbType.VarChar, 100);
                    p_Usuario.Value = Usuario.Usuario;
                    comm.Parameters.Add(p_Usuario);

                    MySqlParameter p_Senha = new MySqlParameter("p_Senha", MySqlDbType.VarChar, 300);
                    p_Senha.Value = Usuario.Senha;
                    comm.Parameters.Add(p_Senha);

                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}