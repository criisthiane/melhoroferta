﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace BitSolutions.Models
{
    public class DashboardContext
    {
        private Int32 _id_Usuario;
        public int Id_Usuario
        {
            get
            {
                return _id_Usuario;
            }

            set
            {
                _id_Usuario = value;
            }
        }

        Int32? _id_Site;
        public int? Id_Site
        {
            get
            {
                return _id_Site;
            }

            set
            {
                _id_Site = value;
            }
        }

        private List<SiteETT> _lstSite = new List<SiteETT>();
        public List<SiteETT> lstSite
        {
            get
            {
                return _lstSite;
            }

            set
            {
                _lstSite = value;
            }
        }

        private List<RankingGoogleETT> _lstRanking = new List<RankingGoogleETT>();
        public List<RankingGoogleETT> lstRanking
        {
            get
            {
                return _lstRanking;
            }

            set
            {
                _lstRanking = value;
            }
        }

        private List<PrecoConcorrenciaETT> _lstPrecoConcorrencia = new List<PrecoConcorrenciaETT>();
        public List<PrecoConcorrenciaETT> lstPrecoConcorrencia
        {
            get
            {
                return _lstPrecoConcorrencia;
            }

            set
            {
                _lstPrecoConcorrencia = value;
            }
        }

        private List<ImagemConcorrenciaETT> _lstImagemConcorrencia = new List<ImagemConcorrenciaETT>();
        public List<ImagemConcorrenciaETT> lstImagemConcorrencia
        {
            get
            {
                return _lstImagemConcorrencia;
            }

            set
            {
                _lstImagemConcorrencia = value;
            }
        }

        public DashboardContext Retorna_Dashboard(Int32 Id_Usuario, Int32? Id_Site)
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["GamaExp"].ConnectionString;
                String strProcedure = "Sp_Sel_Dashboard";
                this.Id_Usuario = Id_Usuario;
                this.Id_Site = Id_Site;

                using (MySqlConnection conn = new MySqlConnection(connString))
                {
                    conn.ConnectionString = connString;

                    conn.Open();
                    MySqlCommand comm = new MySqlCommand(strProcedure, conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    MySqlParameter p_Id_Usuario = new MySqlParameter("p_Id_Usuario", MySqlDbType.Int32);
                    p_Id_Usuario.Value = Id_Usuario;
                    comm.Parameters.Add(p_Id_Usuario);

                    MySqlParameter p_Id_Site = new MySqlParameter("p_Id_Site", MySqlDbType.Int32);
                    p_Id_Site.Value = Id_Site;
                    comm.Parameters.Add(p_Id_Site);

                    MySqlDataReader dr = comm.ExecuteReader();

                    while (dr.Read())
                    {
                        SiteETT ettSite = new SiteETT();
                        ettSite.Id_Site = Convert.ToInt32(dr["Id_Site"]);
                        ettSite.Id_Usuario = Convert.ToInt32(dr["Id_Usuario"]);
                        ettSite.Site = dr["Site"].ToString();
                        ettSite.Dt_Ins = Convert.ToDateTime(dr["Dt_Ins"]);

                        this.lstSite.Add(ettSite);
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            RankingGoogleETT ettGoogle = new RankingGoogleETT();

                            ettGoogle.Id_Ranking_Google = Convert.ToInt32(dr["Id_Ranking_Google"]);
                            ettGoogle.Id_Usuario = Convert.ToInt32(dr["Id_Usuario"]);
                            ettGoogle.Id_Site = Convert.ToInt32(dr["Id_Site"]);
                            ettGoogle.Busca = dr["Busca"].ToString();
                            if (!Convert.IsDBNull(dr["Pos_Atual"]))
                            {
                                ettGoogle.Pos_Atual = dr["Pos_Atual"].ToString();
                            }
                            else
                            {
                                ettGoogle.Pos_Atual = "Sem Posição";
                            }
                            ettGoogle.Pos_Anterior = "Sem Posição";
                            ettGoogle.Dt_Ins = Convert.ToDateTime(dr["Dt_Ins"]);
                            if (!Convert.IsDBNull(dr["Dt_Upd"]))
                            {
                                ettGoogle.Dt_Upd = Convert.ToDateTime(dr["Dt_Upd"]);
                            }

                            this.lstRanking.Add(ettGoogle);
                        }
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            PrecoConcorrenciaETT ettPreco = new PrecoConcorrenciaETT();
                            ettPreco.Id_Usuario = Convert.ToInt32(dr["Id_Usuario"]);
                            ettPreco.Id_Site = Convert.ToInt32(dr["Id_Site"]);
                            ettPreco.Produto = dr["Produto"].ToString();
                            ettPreco.Link_Produto_Usuario = dr["Link_Produto_Usuario"].ToString();
                            ettPreco.Link_Produto_Concorrencia = dr["Link_Produto_Concorrencia"].ToString();
                            if (!Convert.IsDBNull(dr["Preco_Usuario"]))
                            {
                                ettPreco.Preco_Usuario = Convert.ToDecimal(dr["Preco_Usuario"]);
                            }
                            if (!Convert.IsDBNull(dr["Preco_Usuario"]))
                            {
                                ettPreco.Preco_Concorrencia = Convert.ToDecimal(dr["Preco_Concorrencia"]);
                            }

                            ettPreco.Dt_Ins = Convert.ToDateTime(dr["Dt_Ins"]);
                            if (!Convert.IsDBNull(dr["Dt_Upd"]))
                            {
                                ettPreco.Dt_Upd = Convert.ToDateTime(dr["Dt_Upd"]);
                            }

                            this.lstPrecoConcorrencia.Add(ettPreco);
                        }
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            ImagemConcorrenciaETT ettImagem = new ImagemConcorrenciaETT();
                            ettImagem.Id_Usuario = Convert.ToInt32(dr["Id_Usuario"]);
                            ettImagem.Id_Site = Convert.ToInt32(dr["Id_Site"]);
                            ettImagem.Produto = dr["Produto"].ToString();
                            ettImagem.Link_Imagem_Usuario = dr["Link_Imagem_Usuario"].ToString();
                            ettImagem.Link_Imagem_Concorrencia = dr["Link_Imagem_Concorrencia"].ToString();
                            ettImagem.Dt_Ins = Convert.ToDateTime(dr["Dt_Ins"]);
                            if (!Convert.IsDBNull(dr["Dt_Upd"]))
                            {
                                ettImagem.Dt_Upd = Convert.ToDateTime(dr["Dt_Upd"]);
                            }

                            this.lstImagemConcorrencia.Add(ettImagem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return this;
        }

        public Int32 AdicionaSite(SiteETT Site)
        {
            Int32 Id_Site = -1;
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["GamaExp"].ConnectionString;
                String strProcedure = "Sp_Ins_Site";

                using (MySqlConnection conn = new MySqlConnection(connString))
                {
                    conn.ConnectionString = connString;

                    conn.Open();
                    MySqlCommand comm = new MySqlCommand(strProcedure, conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    MySqlParameter p_Id_Usuario = new MySqlParameter("p_Id_Usuario", MySqlDbType.Int32);
                    p_Id_Usuario.Value = Site.Id_Usuario;
                    comm.Parameters.Add(p_Id_Usuario);

                    MySqlParameter p_Site = new MySqlParameter("p_Site", MySqlDbType.VarChar, 200);
                    p_Site.Value = Site.Site;
                    comm.Parameters.Add(p_Site);

                    Id_Site = Convert.ToInt32(comm.ExecuteScalar().ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Id_Site;
        }

        public void AdicionaBuscaGoogle(RankingGoogleETT ranking)
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["GamaExp"].ConnectionString;
                String strProcedure = "Sp_Ins_Ranking_Google";

                using (MySqlConnection conn = new MySqlConnection(connString))
                {
                    conn.ConnectionString = connString;

                    conn.Open();
                    MySqlCommand comm = new MySqlCommand(strProcedure, conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    MySqlParameter p_Id_Usuario = new MySqlParameter("p_Id_Usuario", MySqlDbType.Int32);
                    p_Id_Usuario.Value = ranking.Id_Usuario;
                    comm.Parameters.Add(p_Id_Usuario);

                    MySqlParameter p_Id_Site = new MySqlParameter("p_Id_Site", MySqlDbType.Int32);
                    p_Id_Site.Value = ranking.Id_Site;
                    comm.Parameters.Add(p_Id_Site);

                    MySqlParameter p_Busca = new MySqlParameter("p_Busca", MySqlDbType.VarChar, 400);
                    p_Busca.Value = ranking.Busca;
                    comm.Parameters.Add(p_Busca);

                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AdicionaPrecoConcorrencia(PrecoConcorrenciaETT preco)
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["GamaExp"].ConnectionString;
                String strProcedure = "Sp_Ins_Preco_Concorrencia";

                using (MySqlConnection conn = new MySqlConnection(connString))
                {
                    conn.ConnectionString = connString;

                    conn.Open();
                    MySqlCommand comm = new MySqlCommand(strProcedure, conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    MySqlParameter p_Id_Usuario = new MySqlParameter("p_Id_Usuario", MySqlDbType.Int32);
                    p_Id_Usuario.Value = preco.Id_Usuario;
                    comm.Parameters.Add(p_Id_Usuario);

                    MySqlParameter p_Id_Site = new MySqlParameter("p_Id_Site", MySqlDbType.Int32);
                    p_Id_Site.Value = preco.Id_Site;
                    comm.Parameters.Add(p_Id_Site);

                    MySqlParameter p_Produto = new MySqlParameter("p_Produto", MySqlDbType.VarChar, 400);
                    p_Produto.Value = preco.Produto;
                    comm.Parameters.Add(p_Produto);

                    MySqlParameter p_Link_Produto_Usuario = new MySqlParameter("p_Link_Produto_Usuario", MySqlDbType.VarChar, 400);
                    p_Link_Produto_Usuario.Value = preco.Link_Produto_Usuario;
                    comm.Parameters.Add(p_Link_Produto_Usuario);

                    MySqlParameter p_Link_Produto_Concorrencia = new MySqlParameter("p_Link_Produto_Concorrencia", MySqlDbType.VarChar, 400);
                    p_Link_Produto_Concorrencia.Value = preco.Link_Produto_Concorrencia;
                    comm.Parameters.Add(p_Link_Produto_Concorrencia);

                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AdicionaComparacaoImagem(ImagemConcorrenciaETT imagem)
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["GamaExp"].ConnectionString;
                String strProcedure = "Sp_Ins_Imagem_Concorrencia";

                using (MySqlConnection conn = new MySqlConnection(connString))
                {
                    conn.ConnectionString = connString;

                    conn.Open();
                    MySqlCommand comm = new MySqlCommand(strProcedure, conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    MySqlParameter p_Id_Usuario = new MySqlParameter("p_Id_Usuario", MySqlDbType.Int32);
                    p_Id_Usuario.Value = imagem.Id_Usuario;
                    comm.Parameters.Add(p_Id_Usuario);

                    MySqlParameter p_Id_Site = new MySqlParameter("p_Id_Site", MySqlDbType.Int32);
                    p_Id_Site.Value = imagem.Id_Site;
                    comm.Parameters.Add(p_Id_Site);

                    MySqlParameter p_Produto = new MySqlParameter("p_Produto", MySqlDbType.VarChar, 400);
                    p_Produto.Value = imagem.Produto;
                    comm.Parameters.Add(p_Produto);

                    MySqlParameter p_Link_Imagem_Usuario = new MySqlParameter("p_Link_Imagem_Usuario", MySqlDbType.VarChar, 400);
                    p_Link_Imagem_Usuario.Value = imagem.Link_Imagem_Usuario;
                    comm.Parameters.Add(p_Link_Imagem_Usuario);

                    MySqlParameter p_Link_Imagem_Concorrencia = new MySqlParameter("p_Link_Imagem_Concorrencia", MySqlDbType.VarChar, 400);
                    p_Link_Imagem_Concorrencia.Value = imagem.Link_Imagem_Concorrencia;
                    comm.Parameters.Add(p_Link_Imagem_Concorrencia);

                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}