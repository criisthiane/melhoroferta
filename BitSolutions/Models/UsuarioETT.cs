﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitSolutions.Models
{
    public class UsuarioETT
    {
        private Int32 _id_Usuario;
        private Int32 _id_Site;
        private String _nome;
        private String _sobrenome;
        private String _email;
        private String _estado;
        private String _genero;
        private String _idade;
        private String _usuario;
        private String _senha;

        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }

        public string Sobrenome
        {
            get
            {
                return _sobrenome;
            }

            set
            {
                _sobrenome = value;
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }

        public string Estado
        {
            get
            {
                return _estado;
            }

            set
            {
                _estado = value;
            }
        }

        public string Genero
        {
            get
            {
                return _genero;
            }

            set
            {
                _genero = value;
            }
        }

        public string Idade
        {
            get
            {
                return _idade;
            }

            set
            {
                _idade = value;
            }
        }

        public string Usuario
        {
            get
            {
                return _usuario;
            }

            set
            {
                _usuario = value;
            }
        }

        public string Senha
        {
            get
            {
                return _senha;
            }

            set
            {
                _senha = value;
            }
        }

        public int Id_Usuario
        {
            get
            {
                return _id_Usuario;
            }

            set
            {
                _id_Usuario = value;
            }
        }

        public int Id_Site
        {
            get
            {
                return _id_Site;
            }

            set
            {
                _id_Site = value;
            }
        }
    }
}