﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitSolutions.Models
{
    public class PrecoConcorrenciaETT
    {
        private Int32 _id_Usuario;
        private Int32 _id_Site;
        private String _produto;
        private String _link_Produto_Usuario;
        private String _link_Produto_Concorrencia;
        private Decimal _preco_Usuario;
        private Decimal _preco_Concorrencia;
        private DateTime _dt_Ins;
        private DateTime _dt_Upd;

        public int Id_Usuario
        {
            get
            {
                return _id_Usuario;
            }

            set
            {
                _id_Usuario = value;
            }
        }

        public int Id_Site
        {
            get
            {
                return _id_Site;
            }

            set
            {
                _id_Site = value;
            }
        }

        public string Produto
        {
            get
            {
                return _produto;
            }

            set
            {
                _produto = value;
            }
        }

        public string Link_Produto_Usuario
        {
            get
            {
                return _link_Produto_Usuario;
            }

            set
            {
                _link_Produto_Usuario = value;
            }
        }

        public string Link_Produto_Concorrencia
        {
            get
            {
                return _link_Produto_Concorrencia;
            }

            set
            {
                _link_Produto_Concorrencia = value;
            }
        }

        public decimal Preco_Usuario
        {
            get
            {
                return _preco_Usuario;
            }

            set
            {
                _preco_Usuario = value;
            }
        }

        public decimal Preco_Concorrencia
        {
            get
            {
                return _preco_Concorrencia;
            }

            set
            {
                _preco_Concorrencia = value;
            }
        }

        public DateTime Dt_Ins
        {
            get
            {
                return _dt_Ins;
            }

            set
            {
                _dt_Ins = value;
            }
        }

        public DateTime Dt_Upd
        {
            get
            {
                return _dt_Upd;
            }

            set
            {
                _dt_Upd = value;
            }
        }
    }
}