﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitSolutions.Models
{
    public class RankingGoogleETT
    {
        private Int32 _id_Ranking_Google;
        private Int32 _id_Usuario;
        private Int32 _id_Site;
        private String _busca;
        private String _pos_Atual;
        private String _pos_Anterior;
        private DateTime _dt_Ins;
        private DateTime _dt_Upd;

        public int Id_Usuario
        {
            get
            {
                return _id_Usuario;
            }

            set
            {
                _id_Usuario = value;
            }
        }

        public int Id_Site
        {
            get
            {
                return _id_Site;
            }

            set
            {
                _id_Site = value;
            }
        }

        public string Busca
        {
            get
            {
                return _busca;
            }

            set
            {
                _busca = value;
            }
        }

        public string Pos_Atual
        {
            get
            {
                return _pos_Atual;
            }

            set
            {
                _pos_Atual = value;
            }
        }

        public string Pos_Anterior
        {
            get
            {
                return _pos_Anterior;
            }

            set
            {
                _pos_Anterior = value;
            }
        }

        public DateTime Dt_Ins
        {
            get
            {
                return _dt_Ins;
            }

            set
            {
                _dt_Ins = value;
            }
        }

        public DateTime Dt_Upd
        {
            get
            {
                return _dt_Upd;
            }

            set
            {
                _dt_Upd = value;
            }
        }

        public int Id_Ranking_Google
        {
            get
            {
                return _id_Ranking_Google;
            }

            set
            {
                _id_Ranking_Google = value;
            }
        }
    }
}