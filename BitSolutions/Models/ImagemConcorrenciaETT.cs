﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitSolutions.Models
{
    public class ImagemConcorrenciaETT
    {
        private Int32 _id_Usuario;
        private Int32 _id_Site;
        private String _produto;
        private String _link_Imagem_Usuario;
        private String _link_Imagem_Concorrencia;
        private DateTime _dt_Ins;
        private DateTime _dt_Upd;

        public int Id_Usuario
        {
            get
            {
                return _id_Usuario;
            }

            set
            {
                _id_Usuario = value;
            }
        }

        public int Id_Site
        {
            get
            {
                return _id_Site;
            }

            set
            {
                _id_Site = value;
            }
        }

        public string Produto
        {
            get
            {
                return _produto;
            }

            set
            {
                _produto = value;
            }
        }

        public string Link_Imagem_Usuario
        {
            get
            {
                return _link_Imagem_Usuario;
            }

            set
            {
                _link_Imagem_Usuario = value;
            }
        }

        public string Link_Imagem_Concorrencia
        {
            get
            {
                return _link_Imagem_Concorrencia;
            }

            set
            {
                _link_Imagem_Concorrencia = value;
            }
        }

        public DateTime Dt_Ins
        {
            get
            {
                return _dt_Ins;
            }

            set
            {
                _dt_Ins = value;
            }
        }

        public DateTime Dt_Upd
        {
            get
            {
                return _dt_Upd;
            }

            set
            {
                _dt_Upd = value;
            }
        }
    }
}